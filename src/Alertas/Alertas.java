/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alertas;

import javafx.scene.control.Alert;

/**
 *
 * @author scarlet Espinoza
 */
public class Alertas {
    private Alert alert ;

    public Alertas() {
        this.alert = alert;
    }
    
    public void AlertaAñadir(){
         alert = new Alert(Alert.AlertType.WARNING);
         alert.setTitle("AVL Dialog");
         alert.setHeaderText("Precaución AVL");
         alert.setContentText("El valor ya está añadido");
         alert.showAndWait();
     }
    
    public void AlertaEliminar(){
         alert = new Alert(Alert.AlertType.WARNING);
         alert.setTitle("AVL Dialog");
         alert.setHeaderText("Precaución AVL");
         alert.setContentText("El valor no se encuentra en el arbol");
         alert.showAndWait();
     }
        
     public void AlertaBuscar(){
         alert = new Alert(Alert.AlertType.ERROR);
         alert.setTitle("AVL Dialog");
         alert.setHeaderText("Error AVL ");
         alert.setContentText("No se encuentra valor en el arbol");
         alert.showAndWait();
     }
     
     public void AlertaNoesNumero(){
         alert = new Alert(Alert.AlertType.ERROR);
         alert.setTitle("AVL Dialog");
         alert.setHeaderText("Precaución AVL");
         alert.setContentText("Tipo de dato no aceptado");
         alert.showAndWait();
     }
}
