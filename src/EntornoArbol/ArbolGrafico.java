/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntornoArbol;


import Alertas.Alertas;
import Arbol.Avl;
import Arbol.NodeBT;
import java.io.IOException;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author scarlet Espinoza
 */
public class ArbolGrafico {
    private Alertas alerta = new Alertas(); 
    protected Pane pane = new Pane();
    protected VBox root = new VBox(40);
    protected Text avl = new Text("AVL Tree");
    
    protected NodeBT<Integer> NodoBT;
    protected Avl<Integer> arbol;
    protected double xInicial = 700;
    protected double yInicial = 0;
    
    protected Button AñadirBtn = new Button("  Añadir  ");
    protected TextField AñadirTxt = new TextField();
    protected Button EliminarBtn = new Button(" Eliminar ");
    protected TextField EliminarTxt = new TextField();
    protected Button BuscarBtn = new Button(" Buscar ");
    protected Button Finalizar= new Button("Finalizar");
    protected TextField BuscarTxt = new TextField();
    
    protected HBox AñadirHbox = new HBox(10);
    protected HBox EliminarHbox = new HBox(10);
    protected HBox BuscarHbox = new HBox(10);
    protected HBox GeneralHbox = new HBox(30);
    protected HBox AvlHbox = new HBox();
    protected VBox vacio = new VBox();
    
    public ArbolGrafico(){
        this.arbol =  new Avl<>(Integer::compareTo);
        this.NodoBT=arbol.getRoot();
    }
    
    
    public Scene onPane(){
        AñadirBtn.setLayoutX(0);
        AñadirBtn.setLayoutY(600);
        AñadirTxt.setLayoutX(100);
        AñadirTxt.setLayoutY(600);
        pane.setPrefSize(1400,700);
        
        avl.setFont(Font.loadFont("file:fuente/Mont-Heavy.otf",55));
        avl.setFill(Color.WHITE);
        AvlHbox.setAlignment(Pos.CENTER);
        AñadirBtn.setTextFill(Color.WHITE);
        EliminarBtn.setTextFill(Color.WHITE);
        BuscarBtn.setTextFill(Color.WHITE);
        Finalizar.setTextFill(Color.WHITE);
        GeneralHbox.setAlignment(Pos.CENTER);
        EstiloBoton(AñadirBtn,"#5B2C6F ","#4A235A");
        EstiloBoton(EliminarBtn,"#D4AC0D","#9A7D0A");
        EstiloBoton(BuscarBtn,"#117864","#0B5345");
        EstiloBoton(Finalizar,"#E74C3C","#E74C3C");
        AñadirBtn.setOnAction(e-> AñadirNodo());
        BuscarBtn.setOnAction(e->BuscarNodo());
        EliminarBtn.setOnAction(e->EliminarNodo());
        Finalizar.setOnAction(e->FinalizarAVL());
        AñadirHbox.getChildren().addAll(AñadirBtn,AñadirTxt);
        EliminarHbox.getChildren().addAll(EliminarBtn,EliminarTxt);
        BuscarHbox.getChildren().addAll(BuscarBtn,BuscarTxt);
        GeneralHbox.getChildren().addAll(AñadirHbox,EliminarHbox,BuscarHbox,Finalizar);
        AvlHbox.getChildren().add(avl);
        root.getChildren().addAll(vacio,AvlHbox,GeneralHbox,pane);
        root.setStyle("-fx-background-color: #000000;");
        return new Scene(root,1400,700);
    }
    
    
    private void EstiloBoton(Button b,String color, String shadow){
        String normal= "-fx-background-color: "+color;
        String pressed= "-fx-background-color: "+color+" ; -fx-effect: dropshadow( three-pass-box,"+shadow+ ", 10, 0.0, 0.0, 0.0 )";
        
        b.setStyle(normal);
        
        b.setOnMousePressed(e-> b.setStyle(pressed));
        b.setOnMouseReleased(e-> b.setStyle(normal));
    }
    /**
     * Metodo recursivo que crea un arbol 
     */
     public void CrearArbol(){
        int n = arbol.height()-1;

        CrearArbol(arbol.getRoot(),xInicial,yInicial,n);
        
    }
    private void CrearArbol(NodeBT<Integer> n,double x, double y,int nivel){
        if(n!=null){
            GraficarArbol(n,x,y,nivel);
            --nivel;
            CrearArbol(n.getLeft(),x-((Math.pow(2, nivel)/2)*(80)),y+70,nivel);
            CrearArbol(n.getRight(),x+((Math.pow(2, nivel)/2)*(80)),y+70,nivel);
        }   
    }
    
    /**
     * Metodo que grafica el arbol 
     * @param n R representa el nodo que se insertara
     * @param x la posicion en x
     * @param y la posicion en y
     * @param nivel el nivel en el que se encontrar el arbol al inicio recibe la altura del arbol
     */
    private void GraficarArbol(NodeBT<Integer> n,double x, double y,int nivel){
        NodoGrafico<Integer> nodoGrafico = new NodoGrafico<>(n.getData());
        -- nivel;
        if(n.getLeft()!=null){
            double xi = (x-((Math.pow(2, nivel)/2)*(80)))+30;
            double yi = y+100;
            Line l = new Line();
            l.setStyle("-fx-stroke:#FEFEFE ");
            l.setStartX(x+30);
            l.setStartY(y+30);
            l.setEndX(xi);
            l.setEndY(yi);
            
            pane.getChildren().add(l);
        }
        if(n.getRight()!=null){
            double xi = (x+((Math.pow(2, nivel)/2)*(80)))+30;
            double yi = y+100;
            Line l = new Line();
            l.setStartX(x+30);
            l.setStartY(y+30);
            l.setEndX(xi);
            l.setEndY(yi);
            l.setStyle("-fx-stroke:#FEFEFE ");
            pane.getChildren().add(l);
        }
        
        if(n.isEncontrado()){
            nodoGrafico.getC().setStyle("-fx-fill: #117864; -fx-stroke: #0B5345");
            n.setEncontrado(false);
        }
        nodoGrafico.setLayoutX(x);
        nodoGrafico.setLayoutY(y);
        pane.getChildren().add(nodoGrafico);

    
    }
    
    private void AñadirNodo(){
        try{
           arbol.AñadirAvl(Integer.parseInt(AñadirTxt.getText()));
        }catch(NumberFormatException e){
            alerta.AlertaNoesNumero();
        }
        LimpiarPane();
        AñadirTxt.clear();
        CrearArbol();
    }
    
    private void BuscarNodo(){
        try{
            arbol.BuscarAVL(Integer.parseInt(BuscarTxt.getText()));
        }catch(NumberFormatException e){
            alerta.AlertaNoesNumero();
        }
        BuscarTxt.clear();
        LimpiarPane();
        CrearArbol();
    }
    
    private void EliminarNodo(){
        try{
            arbol.EliminarAVL(Integer.parseInt(EliminarTxt.getText()));
        }catch(NumberFormatException e){
            alerta.AlertaNoesNumero();
        }
        EliminarTxt.clear();
        LimpiarPane();
        CrearArbol();
    }
    private void FinalizarAVL(){
        System.exit(0);
        
    }
    private void LimpiarPane(){
        pane.getChildren().clear();
    }
    
    
}
