/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntornoArbol;

import Arbol.NodeBT;
import java.awt.Font;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

/**
 *
 * @author scarlet Espinoza
 */
public class NodoGrafico <E> extends StackPane{
    Circle c = new Circle(32);
    Text t = new Text();
    NodeBT<E> node;
    
    public NodoGrafico(E data){
        node = new NodeBT(data);
        t.setText(data.toString());
        t.setFill(Color.WHITE);
        t.setStyle("-fx-font-weight: bold");
        c.setStyle("-fx-fill: #000000 ; -fx-stroke:#FEFEFE ");
        
        this.getChildren().addAll(c,t);
    }

    public Circle getC() {
        return c;
    }
}
