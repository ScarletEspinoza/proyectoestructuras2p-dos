/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arbol;

/**
 *
 * @author scarlet Espinoza
 */
public class NodeBT<E> {
    private E dato;
    private NodeBT<E> Izquierda;
    private NodeBT<E> Derecha;
    private boolean Encontrado;
    private int fe;

    public NodeBT(E data) {
        this.dato = data;
        Izquierda = Derecha=null;
        fe = 0;
        Encontrado = false;
    }

    public E getData() {
        return dato;
    }

    public boolean isEncontrado() {
        return Encontrado;
    }

    public void setEncontrado(boolean Encontrado) {
        this.Encontrado = Encontrado;
    }
   
    public void setData(E data) {
        this.dato = data;
    }

    public NodeBT<E> getLeft() {
        return Izquierda;
    }

    public void setLeft(NodeBT<E> left) {
        this.Izquierda = left;
    }

    public NodeBT<E> getRight() {
        return Derecha;
    }

    public void setRight(NodeBT<E> right) {
        this.Derecha = right;
    }

    public int getF() {
        return fe;
    }

    public void setF(int f) {
        this.fe = f;
    }
    @Override
    public String toString() {
        return "NodeBT{" + "data=" + dato + '}';
    }
    
}
