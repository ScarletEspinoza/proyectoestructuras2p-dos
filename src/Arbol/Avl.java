/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arbol;

import Alertas.Alertas;
import java.util.Comparator;
import javafx.scene.control.Alert;
import javax.swing.JOptionPane;

/**
 *
 * @author scarlet Espinoza
 */
public class Avl<E> {
    private NodeBT<E> root;
    private Comparator<E> f;
    private Logical ValorBooleano; // este tambien podria haber sido de tipo de dato boolean en realidad no neceariamente Logical
    private Alertas alerta = new Alertas(); // este campo no forma parte del AVL solo  lo añadi para majear las alertas 
    public Avl(Comparator<E> f){
        root=null;
        this.f=f;
        ValorBooleano= new Logical(false);
    }
    public boolean isEmpty(){
        return root==null;
    }
    
    public int height(){
        return height(root);
    }
    
    private int height(NodeBT<E> p){
        if(p==null)
            return 0;
        else if(p.getLeft()==null && p.getRight()==null)
            return 1;
        int l = height(p.getLeft());
        int r = height(p.getRight());
        return  l>r ? 1+l: r+1;
    }
     public int size(){
        return size(root);
    }
    
    private int size(NodeBT<E> p){
        return (p==null)?0:1+size(p.getLeft())+size(p.getRight());
    }

    public NodeBT<E> getRoot() {
        return root;
    }

    public void setRoot(NodeBT<E> root) {
        this.root = root;
    }

    public Comparator<E> getF() {
        return f;
    }

    public void setF(Comparator<E> f) {
        this.f = f;
    }
    
    private NodeBT<E> rotacionDerechaDerecha(NodeBT<E> n,NodeBT<E> n1){            
                n.setRight(n1.getLeft());
                n1.setLeft(n);
                if(n1.getF()==1){
                    n1.setF(0);
                    n.setF(0);
                }else{
                    n.setF(1);
                    n1.setF(-1);
                }
        return n1;
        
    }
    
    private NodeBT<E> rotacionIzquierdaIzquierda(NodeBT<E> n,NodeBT<E> n1){
                n.setLeft(n1.getRight());
                n1.setRight(n);
                if(n1.getF()==-1){
                    n.setF(0);
                    n1.setF(0);
                }else{
                    n.setF(-1);
                    n1.setF(1);
                }
        return n1;
        
    }
    private NodeBT<E> rotacionIzquierdaDerecha(NodeBT<E> n,NodeBT<E> n1){
        NodeBT<E> n2 = n1.getRight();
        n.setLeft(n2.getRight());
        n2.setRight(n);
        n1.setRight(n2.getLeft());
        n2.setLeft(n1);
        if(n2.getF()==1){
            n1.setF(-1);
        }else{
            n1.setF(0);
        }if(n2.getF()==-1){
            n.setF(1);
        }else{
            n.setF(0);
        }
        n2.setF(0);
        return n2;
    }
    
    private NodeBT<E> rotacionDerechaIzquierda(NodeBT<E> n, NodeBT<E> n1){
        NodeBT<E> n2 = n1.getLeft();
        n.setRight(n2.getLeft());
        n2.setLeft(n);
        n1.setLeft(n2.getRight());
        n2.setRight(n1);
        if(n2.getF()==1){
            n.setF(-1);
        }else{
            n.setF(0);
        }
        if(n2.getF() == -1){
            n1.setF(1);
        }else{
            n1.setF(0);
        }
        n2.setF(0);    
        return n2;
    }
     
    public boolean AñadirAvl(E data){
        if(data==null)
            return false;
        try {
            this.root=AñadirAVL(data,root);
        } catch (Exception ex) {
            
            return false;
        }
        return true;
    }
    
    private NodeBT<E> AñadirAVL(E data, NodeBT<E> n) throws Exception{
        if(n==null){
            n=new NodeBT<>(data);
            ValorBooleano.setLogical(true);
        }else if(f.compare(data, n.getData())<0){
            n.setLeft(AñadirAVL(data,n.getLeft()));
            if(ValorBooleano.booleanValue()){
                switch(n.getF()){
                case 1:
                    n.setF(0);
                    ValorBooleano.setLogical(false);
                    break;
                case 0:
                    n.setF(-1);
                    break;
                case -1:
                    NodeBT<E> n1 = n.getLeft();
                    if(n1.getF()==-1){
                        n = rotacionIzquierdaIzquierda(n,n1);
                    }else{
                       n = rotacionIzquierdaDerecha(n,n1);
                    }
                    ValorBooleano.setLogical(false);            
            }}     
        }else if(f.compare(data, n.getData())>0){
                n.setRight(AñadirAVL(data,n.getRight()));
                if(ValorBooleano.booleanValue()){
                switch(n.getF()){
                    case 1:
                        NodeBT<E> n1 = n.getRight();
                        if(n1.getF()==1){
                            n = rotacionDerechaDerecha(n,n1);
                        }else{
                            n = rotacionDerechaIzquierda(n,n1);
                        }
                        ValorBooleano.setLogical(false);
                        break;
                    case 0:
                        n.setF(1);
                        break;
                    case -1:
                        n.setF(0);
                        ValorBooleano.setLogical(false);
                    
                    
            }}
        
            
        }else{
            alerta.AlertaAñadir();
            throw new Exception("No puede haber claves repetidas");
        }
        
        return n;
  } 
    
    
    public boolean BuscarAVL (E data){
        try{
            NodeBT<E> node = BuscarAVL(root,data);
            node.setEncontrado(true);
        }catch(Exception e){
            alerta.AlertaBuscar();
            return false;
        }
        return true;
    }
    
    private NodeBT<E> BuscarAVL(NodeBT<E> n,E data) throws Exception{
        if(n==null)
            throw new Exception("");
        else if(f.compare(data, n.getData())<0){
            return BuscarAVL(n.getLeft(),data);
        }else if (f.compare(data, n.getData())>0){
            return BuscarAVL(n.getRight(),data);
        }else{
            
            return n;
        }
        
    }
    
    public boolean EliminarAVL(E data){
        Logical flag = new Logical(false);
        if(data==null)
            return false;
        try{
            this.root = EliminarAVL(root,data,flag);
        }catch(Exception e){
            alerta.AlertaEliminar();
            return false;
        }
        
        return true;
    }
   
     private NodeBT<E> EliminarAVL(NodeBT<E> node,E data, Logical cambiaAltura) throws Exception{
        if(node==null)
            throw new Exception("");
        else if(f.compare(data, node.getData())<0){
            NodeBT<E> left;
            left = EliminarAVL (node.getLeft(),data,cambiaAltura);
            node.setLeft(left);
            if(cambiaAltura.booleanValue())
                node = equilibrarIzquierda(node, cambiaAltura);
        }
        else if(f.compare(data, node.getData())>0){
            NodeBT<E> right;
            right = EliminarAVL(node.getRight(), data, cambiaAltura);
            node.setRight(right);
            if(cambiaAltura.booleanValue())
                node = equilibrarDerecha(node, cambiaAltura);
        }
        else{   // Nodo encontrado   
            NodeBT<E> n = node;
            if(n.getLeft() == null){
                node = n.getRight();
                cambiaAltura.setLogical(true);
            }
            else if(n.getRight() == null){
                node = n.getLeft();
                cambiaAltura.setLogical(true);
            }
            else{   // Tiene rama izquierda y derecha
                NodeBT<E> left;
                left = reemplazar(node, node.getLeft(), cambiaAltura);
                node.setLeft(left);
                if(cambiaAltura.booleanValue())
                    node = equilibrarIzquierda(node, cambiaAltura);
            }
            n = null;
        }
        return node;
    }
     
    private NodeBT<E> reemplazar(NodeBT<E> node, NodeBT<E> actual, Logical cambiaAltura){
        if (actual.getRight() != null){
            NodeBT<E> right;
            right = reemplazar(node, actual.getRight(), cambiaAltura);
            actual.setRight(right);
            if (cambiaAltura.booleanValue())
                actual = equilibrarDerecha(actual, cambiaAltura);
        }
        else{
            node.setData(actual.getData());
            node = actual;
            actual = actual.getLeft();
            node = null;
            cambiaAltura.setLogical(true);
        }
        return actual;
    } 
     
    private NodeBT<E> equilibrarIzquierda(NodeBT<E> node, Logical cambiaAltura){
        NodeBT<E> n1;
        switch (node.getF()){
            case -1 :
                node.setF(0);
                break;
            case 0 :
                node.setF(1);
                cambiaAltura.setLogical(false);
                break;
            case 1 : //se aplica un tipo de rotación derecha
                n1 = node.getRight();
                if (n1.getF() >= 0){
                    if (n1.getF() == 0){ //la altura no vuelve a disminuir
                        cambiaAltura.setLogical(false);
                    }
                    node = rotacionDerechaDerecha(node, n1);
                }
                else{
                    node = rotacionDerechaIzquierda(node, n1);
                }break;
        }
        return node;
    }
    
    private NodeBT<E> equilibrarDerecha(NodeBT<E> node, Logical cambiaAltura){
        NodeBT<E> n1;
        switch (node.getF()){
            case -1:// Se aplica un tipo de rotación izquierda
                n1 = node.getLeft();
                if (n1.getF() <= 0){
                    if (n1.getF() == 0){
                        cambiaAltura.setLogical(false);
                     }
                    node = rotacionIzquierdaIzquierda(node, n1);
                }else{
                    node = rotacionIzquierdaDerecha(node,n1);
                }
                break;
            case 0 : 
                node.setF(-1);
                cambiaAltura.setLogical(false);
                break;
            case 1 : 
                node.setF(0);
                break;
         }
         return node;
    }
    
}