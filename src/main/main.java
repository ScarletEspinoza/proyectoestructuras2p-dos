/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import EntornoArbol.ArbolGrafico;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author scarlet Espinoza
 */
public class main  extends Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        launch();
    }
    
    public void start(Stage stage){
        stage.setTitle("AVL tree");
        ArbolGrafico arbol = new ArbolGrafico();
        Scene s = arbol.onPane();
        stage.setScene(s);
        stage.show();
    }
    
}
